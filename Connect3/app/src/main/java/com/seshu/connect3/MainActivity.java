package com.seshu.connect3;

import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import android.media.MediaPlayer;

public class MainActivity extends AppCompatActivity {

    //active player: 0 = yellow; 1 =red

    int activePlayer = 0;
    int[] gameState = {2,2,2,2,2,2,2,2,2};
    int currentPosition=0;
    int[][] winningPositions = {{0,1,2},{3,4,5},{6,7,8},{0,3,6},{1,4,7},{2,5,8},{0,4,8},{2,4,6}};
    android.widget.GridLayout ticTacLayout;
    MediaPlayer mediaPlayer;

    public void dropIn(View view)
    {
        ImageView counter = (ImageView) view;
        currentPosition =  Integer.parseInt(counter.getTag().toString());

        if(gameState[currentPosition] == 2)
        {
            counter.setTranslationY(-1000f);
            if (activePlayer == 0)
            {
                counter.setImageResource(R.drawable.yellowicon);
                gameState[currentPosition] = activePlayer;
                activePlayer = 1;
            }
            else if (activePlayer == 1)
            {
                counter.setImageResource(R.drawable.redicon);
                gameState[currentPosition] = activePlayer;
                activePlayer = 0;
            }

            counter.animate()
                    .translationYBy(1000f)
                    .rotation(180)
                    .translationZ(200f)
                    .setDuration(500);

            checkWinner();
        }
    }

    private void checkWinner()
    {
        boolean isAPlayerWinner = true;
        boolean isBPlayerWinner = true;
        boolean isDraw = false;
        boolean isGameOver = true;

        for(int[] positionArray: winningPositions) {
            isAPlayerWinner = true;
            isBPlayerWinner = true;
            isGameOver = true;

            for (int i = 0; i < gameState.length; i++) {
                if (gameState[i] == 2) {
                    isGameOver = false;
                }
            }

            if (isGameOver == false) {

                for (int i = 0; i < positionArray.length; i++) {
                    if (gameState[positionArray[i]] != 0) {
                        isAPlayerWinner = false;
                    }

                    if (gameState[positionArray[i]] != 1) {
                        isBPlayerWinner = false;
                    }
                }

                if (isAPlayerWinner == true) {
                    Toast.makeText(this, "Game Over!!!!!!!!", Toast.LENGTH_SHORT).show();
                    Toast.makeText(this, "Hurray! Player Yellow is the winner", Toast.LENGTH_LONG).show();
                    ticTacLayout.animate().rotation(3600f).setDuration(2000);
                    resetGame();
                }

                if (isBPlayerWinner == true) {
                    Toast.makeText(this, "Game Over!!!!!!!!", Toast.LENGTH_SHORT).show();
                    Toast.makeText(this, "Hurray! Player Red is the winner", Toast.LENGTH_LONG).show();
                    ticTacLayout.animate().rotation(3600f).setDuration(2000);
                    resetGame();
                }
            }
            else{
                Toast.makeText(this, "Game Over!!!!!!!!", Toast.LENGTH_SHORT).show();
                Toast.makeText(this, "It is a draw game !!!!!!!", Toast.LENGTH_LONG).show();
                ticTacLayout.animate().rotation(3600f).setDuration(2000);
                resetGame();
            }
        }

    }

    private void resetGame()
    {
        activePlayer = 0;
        for(int i=0; i <gameState.length; i++)
        {
            gameState[i] = 2;
        }
        for(int i=0; i<ticTacLayout.getChildCount(); i++)
        {
            ((ImageView) ticTacLayout.getChildAt(i)).setImageResource(0);
        }
        mediaPlayer.start();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ticTacLayout = (android.widget.GridLayout) findViewById(R.id.gridLayout);
        mediaPlayer = MediaPlayer.create(this,R.raw.banana);
    }
}
